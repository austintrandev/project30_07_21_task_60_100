package com.devcamp.person;

import java.util.ArrayList;

import com.devcamp.pets.CPet;
import com.devcamp.vehicles.*;

public class CPerson {

	public CPerson(int id, int age, String firstName, String lastName, String country) {
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.country = country;

		this.vehicles = new ArrayList<CVehicle>();
		this.pets = new ArrayList<CPet>();

	}

	public CPerson() {
		// TODO Auto-generated constructor stub
	}

	private int id;
	private int age;
	private String firstName;
	private String lastName;
	private ArrayList<CVehicle> vehicles;
	private ArrayList<CPet> pets;
	private String country;
	
	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}
	
	public void addVehicle(CVehicle paramVehicle) {
		vehicles.add(paramVehicle);
	}
	
	public void addPet(CPet paramPet) {
		pets.add(paramPet);
	}

	private void printAllPets() {
		System.out.println("---All Pets---");
		for (CPet vPet : pets) {
			vPet.print();
		}
	}
	
	private void printAllVehicles() {
		System.out.println("---All Vehicles---");
		for (CVehicle vVehicle : vehicles) {
			vVehicle.print();
		}
	}
	
	public void printPerson() {
		System.out.println("---Person info---");
		System.out.println("PersonId: " + this.id);
		System.out.println("Fullname: " + this.getFullName());
		System.out.println("Person age: " +  this.age);
		System.out.println("Person country: " + this.country);
		this.printAllVehicles();
		this.printAllPets();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public ArrayList<CVehicle> getVehicles() {
		return vehicles;
	}

	public ArrayList<CPet> getPets() {
		return pets;
	}

}
