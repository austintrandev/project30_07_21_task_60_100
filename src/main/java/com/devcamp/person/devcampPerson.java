package com.devcamp.person;

import com.devcamp.pets.*;
import com.devcamp.vehicles.*;

public class devcampPerson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CPet vMyPet = new CPet(2,"Momo");
		CCat vMyCat = new CCat(2,"Mimi");
		CDog vMyDog = new CDog(1,"LuLu");
		
		CCar vCar = new CCar("SC40", "Blue", "59G-34589", "Volvo", 2020);
		CMotorbike vMotorbike = new CMotorbike("Auto", "120cc", "Honda", 2020);
		CVehicle vVehicle = new CCar ("SC60", "Red", "59G-68336","Volvo",2021);
		
		CPerson myPerson = new CPerson(12, 25, "Harry", "Potter", "UK");
		
		myPerson.addVehicle(vCar);
		myPerson.addVehicle(vMotorbike);
		
		myPerson.addPet(vMyDog);
		myPerson.addPet(vMyCat);
		
		myPerson.printPerson();
	}

}
