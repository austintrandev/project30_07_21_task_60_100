package com.devcamp;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.person.CPerson;
import com.devcamp.pets.*;
import com.devcamp.vehicles.*;



@RestController
public class personController {
	@CrossOrigin
	@GetMapping("/people")
	public ArrayList<CPerson> getPersonList() {
		ArrayList<CPerson> vPersonList = new ArrayList<CPerson>();

		CPet vMyPet = new CPet(2, "Momo");
		CCat vMyCat = new CCat(2, "Mimi");
		CDog vMyDog = new CDog(1, "LuLu");

		CCar vCar = new CCar("SC40", "Blue", "59G-34589", "Volvo", 2020);
		CMotorbike vMotorbike = new CMotorbike("Auto", "120cc", "Honda", 2020);
		CPerson myPerson = new CPerson(12, 25, "Harry", "Potter", "UK");

		myPerson.addVehicle(vCar);
		myPerson.addVehicle(vMotorbike);

		myPerson.addPet(vMyDog);
		myPerson.addPet(vMyCat);

		vPersonList.add(myPerson);
		
		return vPersonList;
	}
}
