package com.devcamp.pets;

public class CCat extends CPet {

	public CCat() {
		// TODO Auto-generated constructor stub
	}

	public CCat(int age, String name) {
		super(age, name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void say() {
		System.out.println("Cat saying");
	}

	@Override
	public void print() {
		System.out.println("---Cat info---");
		System.out.println("Cat name: " + this.name);
		System.out.println("Cat age: " + this.age);
	}

	@Override
	public void play() {
		System.out.println("Cat palying");
	}

}
