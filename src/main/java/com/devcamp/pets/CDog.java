package com.devcamp.pets;

public class CDog extends CPet {

	public CDog() {
		// TODO Auto-generated constructor stub
	}
	
	public CDog(int age, String name) {
		super(age, name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void say() {
		System.out.println("Dog saying");
	}

	@Override
	public void print() {
		System.out.println("---Dog info---");
		System.out.println("Dog name: " + this.name);
		System.out.println("Dog age: " + this.age);
	}

	@Override
	protected void play() {
		System.out.println("Dog palying");
	}
}
