package com.devcamp.pets;

public class CPet {
	public CPet() {

	}

	public CPet(int age, String name) {
		this.age = age;
		this.name = name;
	}

	protected int age;
	protected String name;

	protected void say() {
		System.out.println("Pet saying");
	}

	public void print() {
		System.out.println("---Pet info---");
		System.out.println("Pet name: " + this.name);
		System.out.println("Pet age: " + this.age);
	}

	protected void play() {
		System.out.println("Pet palying");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
