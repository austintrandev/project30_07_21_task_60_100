package com.devcamp.vehicles;

public class CCar extends CVehicle {
	public CCar() {
		super();
	}

	public CCar(String modalName, String color, String vId, String paramBrand, int paramYearManufactured) {
		super(paramBrand, paramYearManufactured);
		this.modalName = modalName;
		Color = color;
		this.vId = vId;

	}

	private String modalName;
	private String Color;
	private String vId;

	public String getModalName() {
		return modalName;
	}

	public void setModalName(String modalName) {
		this.modalName = modalName;
	}

	public String getColor() {
		return Color;
	}

	public void setColor(String color) {
		Color = color;
	}

	public String getvId() {
		return vId;
	}

	public void setvId(String vId) {
		this.vId = vId;
	}

	@Override
	public int maxSpeedPerKm() {
		return 200;
	}

	@Override
	public void print() {
		System.out.println("---- Car info ----");
		System.out.println("Brand is: " + this.brand);
		System.out.println("yearManufactured is: " + this.yearManufactured);
	}

}
