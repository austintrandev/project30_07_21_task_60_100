package com.devcamp.vehicles;

public class CMotorbike extends CVehicle {
	public CMotorbike() {
		super();
	}

	public CMotorbike(String kind, String power, String paramBrand, int paramYearManufactured) {
		super(paramBrand, paramYearManufactured);
		this.kind = kind;
		this.power = power;
	}

	private String kind;
	private String power;

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public String getPower() {
		return power;
	}

	public void setPower(String power) {
		this.power = power;
	}

	@Override
	public int maxSpeedPerKm() {
		return 150;
	}

	@Override
	public void print() {
		System.out.println("---- Motorbike info ----");
		System.out.println("Brand is: " + this.brand);
		System.out.println("yearManufactured is: " + this.yearManufactured);
	}

}
