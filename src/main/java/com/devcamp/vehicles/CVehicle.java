package com.devcamp.vehicles;

public class CVehicle {
	public CVehicle() {
		
	}

	public CVehicle(String brand, int yearManufactured) {
		this.brand = brand;
		this.yearManufactured = yearManufactured;
	}

	protected String brand;
	protected int yearManufactured;

	public int maxSpeedPerKm() {
		return 100;
	}

	public void print() {
		System.out.println("---- Vehicle info ----");
		System.out.println("Brand is: " + this.brand);
		System.out.println("yearManufactured is: " + this.yearManufactured);
	}
	
	public void print(String paramVehicleType) {
		System.out.println("----" +  paramVehicleType + " info----");
		System.out.println("Brand is: " + this.brand);
		System.out.println("yearManufactured is: " + this.yearManufactured);
	}

	public void honk() {
		System.out.println("Beep beep!!");
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public int getYearManufactured() {
		return yearManufactured;
	}

	public void setYearManufactured(int yearManufactured) {
		this.yearManufactured = yearManufactured;
	}
}
